import axios from 'axios'
import qs from 'qs'
import { getCookie } from '@/common/js/cookie'
import router from '../router'
axios.defaults.timeout = 10000;                        //响应时间
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';//配置请求头
axios.defaults.baseURL = '';   //配置接口地址

//POST传参序列化(添加请求拦截器)
axios.interceptors.request.use((config) => {	
	//在发送请求之前做某件事
	if(!config.data){config.data = {}}
	const userId = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
	const sstoken = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
	if(config.url.indexOf('/tryout/app/client') > -1){
		if(config.method  === 'get'){
			config.params['vipCode_userId'] = userId
			config.params['vipCode_domainName'] = 'Haier'
			config.params['vipCode_ssotoken'] = sstoken
		}
		return config
	}
	if(config.url.indexOf('/tryout/upload') > -1){
		axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';//配置请求头
		console.log(config)
		return config
	}
	config.data['vipCode_userId'] = userId
	config.data['vipCode_domainName'] = 'Haier'
	config.data['vipCode_ssotoken'] = sstoken
	if(config.method  === 'post'){
		if (Object.prototype.toString.call(config.data) != '[object FormData]') {
			// 请求拦截器处理
			config.data = qs.stringify(config.data);
		}
	}
	return config;
},(error) =>{
		console.log('错误的传参')
    return Promise.reject(error);
});

//返回状态判断(添加响应拦截器)
axios.interceptors.response.use((res) =>{
	//对响应数据做些事
	if(!res.data.success){
		return Promise.resolve(res);
	}
	return res;
}, (error) => {
	const { response } = error
	if (response) {
		return Promise.reject(error);
	}else if(navigator.onLine){
		return Promise.reject(error);
	} else {
		router.push('networkFail')
	}
});

export function fetchPost(url, data) {
	const userId = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
	const sstoken = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
	const param = {
		vipCode_userId: userId,
		vipCode_domainName: 'Haier',
		vipCode_ssotoken: sstoken
	}
	const params = '?' + formateObjToParamStr(param)
	return new Promise((resolve, reject) => {
		axios.post(url + params, data)
		.then(response => {
			resolve(response.data);
		}, err => {
			reject(err);
		})
		.catch((error) => {
			reject(error)
		})
	})
}

export function fetchGet(url, param) {
	return new Promise((resolve, reject) => {
		axios.get(url, {params: param}, {
			headers: {
				'Access-Control-Allow-Origin':'*',  //解决cors头问题
				'Access-Control-Allow-Credentials':'true', //解决session问题
			}
		})
			.then(response => {
				resolve(response.data)
			}, err => {
				reject(err)
			})
			.catch((error) => {
				reject(error)
			})
	})
}
export function fetchPut(url, data) {
	const userId = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
	const sstoken = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
	const param = {
		vipCode_userId: userId,
		vipCode_domainName: 'Haier',
		vipCode_ssotoken: sstoken
	}
	const params = '?' + formateObjToParamStr(param)
	return new Promise((resolve, reject) => {
		axios.put(url + params, data, {
			headers: {
				'Access-Control-Allow-Origin':'*',  //解决cors头问题
				'Access-Control-Allow-Credentials':'true', //解决session问题
				'Content-Type': 'application/json;charset=UTF-8'
			}
		})
		.then(response => {
			resolve(response.data);
		}, err => {
			reject(err);
		})
		.catch((error) => {
			reject(error)
		})
	})
}
function filter(str) { // 特殊字符转义
  str += ''; // 隐式转换
  str = str.replace(/%/g, '%25');
  str = str.replace(/\+/g, '%2B');
  str = str.replace(/ /g, '%20');
  str = str.replace(/\//g, '%2F');
  str = str.replace(/\?/g, '%3F');
  str = str.replace(/&/g, '%26');
  str = str.replace(/\=/g, '%3D');
  str = str.replace(/#/g, '%23');
  return str;
}

function formateObjToParamStr(paramObj) {
  const sdata = [];
  for (let attr in paramObj) {
    sdata.push(`${attr}=${filter(paramObj[attr])}`);
  }
  return sdata.join('&');
}

export default {
	fetchPost,
	fetchGet,
	fetchPut
}