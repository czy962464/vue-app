
// import { isUhomeApp } from '@/common/js/isApp'
// if(isUhomeApp()){
  // let uplusApi = new UplusApi()
// }
import Vue from 'vue'
// import qs from 'qs'
import { getCookie } from '@/common/js/cookie'
export function fetchGet(url, params) {
  let formData = params
  if(!formData){formData = {}}
	formData['vipCode_userId'] = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
	formData['vipCode_domainName'] = getCookie('vipCode_domainName') ? getCookie('vipCode_domainName') : ''
	formData['vipCode_ssotoken'] = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
  let param = formateObjToParamStr(formData)
  return new Promise((resolve, reject) => {
    Vue.prototype.$uplusApi.initDeviceReady().then(() => {
      Vue.prototype.$uplusApi.upHttpModule.get({
        url: url + "?" + param,
        headers:{},
        transform: true
      }).then(response => {
        if(response.retCode == "000000"){
          // console.log(response)
          resolve(response.retData.data)
        }else{
          reject(response)
        }
      }).catch((err) =>{
        reject(err)
      })
    })
	})
}
export function fetchPost(url, data) {
  let formData = data
  if(!formData){formData = {}}
  formData['vipCode_userId'] = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
  formData['vipCode_domainName'] = getCookie('vipCode_domainName') ? getCookie('vipCode_domainName') : ''
  formData['vipCode_ssotoken'] = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
  let param = '?' + formateObjToParamStr(formData)
  console.log("post请求数据")
  console.log(param)
  console.log(Vue.prototype.$uplusApi)
	return new Promise((resolve, reject) => {
    Vue.prototype.$uplusApi.initDeviceReady().then(() => {
      Vue.prototype.$uplusApi.upHttpModule.post({
        url: url + param,
        headers:{"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"},
        data: {},
        transform: true
      }).then(response => {
        // console.log("post返回数据")
        // console.log(response)
        if(response.retCode == "000000"){
          resolve(response.retData.data)
        }else{
          reject(response)
        }
      }).catch((err) =>{
        console.log('封装错误')
        console.log(err)
        console.log(url + param)
        reject(err)
      })
    })
  })
}
export function fetchUpload(url, data) {
  let formData = ''
  if(!formData){formData = {}}
	formData['vipCode_userId'] = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
	formData['vipCode_domainName'] = getCookie('vipCode_domainName') ? getCookie('vipCode_domainName') : ''
	formData['vipCode_ssotoken'] = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
  let param = '?' + formateObjToParamStr(formData)
  console.log("uploadFile请求数据")
  let request = {
    url: url + param,
    headers:{"Content-Type": "multipart/form-data"},
    data: {},
    filePath: data,
    transform: true
  }
	return new Promise((resolve, reject) => {
    Vue.prototype.$uplusApi.upHttpModule.uploadFile(request).then(response => {
      console.log("uploadFile返回数据")
      console.log(response)
      if(response.retCode == "000000"){
        resolve(response.retData.data)
      }else{
        reject(response)
      }
    }).catch((err) =>{
      console.log('封装错误')
      console.log(err)
      reject(err)
    })
  })
}
export function fetchPostJson(url, data) {
  let formData = {}//data
  if(!formData){formData = {}}
  const userId = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
	const sstoken = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
  formData['vipCode_userId'] = userId
  formData['vipCode_domainName'] = 'Haier'
  formData['vipCode_ssotoken'] = sstoken
  let param = '?' + formateObjToParamStr(formData)
  console.log("postjson请求数据")
  console.log(param)
  console.log(Vue.prototype.$uplusApi)
	return new Promise((resolve, reject) => {
    Vue.prototype.$uplusApi.initDeviceReady().then(() => {
      Vue.prototype.$uplusApi.upHttpModule.post({
        url: url + param,
        headers:{"Content-Type": "application/json;charset=UTF-8"},
        data: data,
        transform: true
      }).then(response => {
        // console.log("post返回数据")
        // console.log(response)
        if(response.retCode == "000000"){
          resolve(response.retData.data)
        }else{
          reject(response)
        }
      }).catch((err) =>{
        console.log('封装错误')
        console.log(err)
        console.log(url + param)
        reject(err)
      })
    })
  })
}

export function fetchPut(url, data) {
  let formData = {}//data
  if(!formData){formData = {}}
  const userId = getCookie('vipCode_userId') ? getCookie('vipCode_userId') : ''
	const sstoken = getCookie('vipCode_ssotoken') ? getCookie('vipCode_ssotoken') : ''
  formData['vipCode_userId'] = userId
  formData['vipCode_domainName'] = 'Haier'
  formData['vipCode_ssotoken'] = sstoken
  let param = '?' + formateObjToParamStr(formData)
	return new Promise((resolve, reject) => {
    Vue.prototype.$uplusApi.initDeviceReady().then(() => {
      Vue.prototype.$uplusApi.upHttpModule.put({
        url: url + param,
        headers:{"Content-Type": "application/json;charset=UTF-8"},
        data: data,
        transform: true
      }).then(response => {
        if(response.retCode == "000000"){
          resolve(response.retData.data)
        }else{
          reject(response)
        }
      }).catch((err) =>{
        console.log('封装错误')
        console.log(err)
        console.log(url + param)
        reject(err)
      })
    })
  })
}

function filter(str) { // 特殊字符转义
  str += ''; // 隐式转换
  str = str.replace(/%/g, '%25');
  str = str.replace(/\+/g, '%2B');
  str = str.replace(/ /g, '%20');
  str = str.replace(/\//g, '%2F');
  str = str.replace(/\?/g, '%3F');
  str = str.replace(/&/g, '%26');
  str = str.replace(/\=/g, '%3D');
  str = str.replace(/#/g, '%23');
  return str;
}

function formateObjToParamStr(paramObj) {
  const sdata = [];
  for (let attr in paramObj) {
    sdata.push(`${attr}=${filter(paramObj[attr])}`);
  }
  return sdata.join('&');
};
export default {
  fetchPost,
  fetchGet,
  fetchUpload,
  fetchPostJson,
  fetchPut
}