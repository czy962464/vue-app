import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/login.vue' //断网默认页
import networkFail from '@/views/networkFail.vue' //断网默认页
// const addAddress = () => import(/* webpackChunkName: 'setup' */ '../modules/view/setup/addAddress.vue')

Vue.use(Router)
//路由数组
const routerList = []
function importAll(r){
    r.keys().forEach(
        (key) => {
            r(key).default.forEach(index => {
                routerList.push(index)
            })
        }
    )
}
importAll(require.context('.', true, /\.routes\.js/))
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const rootPath = ''
export default new Router({
    mode: 'hash',
    scrollBehavior(to, from, savedPosition) {
        if(savedPosition) {
            return savedPosition
        }
        return {
            x: 0,
            y: 0
        }
    },
    routes: [
        ...routerList,
        {
            path: rootPath + '/',
            name: 'Login',
            component: Login,
            meta:{
                title: '登录',
                keepAlive: false
            }
        },
        {
            path: rootPath + '/networkFail',
            name: 'networkFail',
            component: networkFail,
            meta:{
                title: '无网络',
                keepAlive: false
            }
        }
    ]
})