// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
// import 'lib-flexible/flexible' // px转化rem
import { setCookie, getCookie, delCookie } from '@/common/js/cookie'
import { fetchGet, fetchPost } from '@/utils/http.js'
// import { fetchPut } from '@/utils/uhome_http.js'
// import { uhomePageTrack, uhomeChangeTracker, uhomeClickTracker } from "@/common/js/uhome-tracker.js"
// import "@/common/js/direction"
// import '@/common/css/reset.css'
import Vant from 'vant'
import 'vant/lib/index.css'
Vue.use(Vant)

Vue.prototype.$fetchPost = fetchPost
Vue.prototype.$fetchGet = fetchGet
window.$setCookie = setCookie
window.$getCookie = getCookie
window.$delCookie = delCookie

window.APPID = 'wx2409ae17ef4a9f34';
window.WXID = 'gh_735017156c3f'
window.TESTBASE = 'https://hzy.haier.com/vipCode'
window.vueReferer = ''


//挂载公共头部
// import newHeader from "@/common/components/header.vue"
// Vue.component(newHeader.name, newHeader)
//领取勋章、成长值接口封装
// import { handleReciveGrowth, handleUnlockMedal } from '@/common/js/growthReceive'
// Vue.prototype.$handleReciveGrowth = handleReciveGrowth
// Vue.prototype.$handleUnlockMedal = handleUnlockMedal
//2020-08-17;author: czy;function:全局注册公共弹窗组件
// import { SmallMaskPlugin, LoadingPlugin } from '@/common/js/commonPlugin'
// Vue.use(SmallMaskPlugin)
// Vue.use(LoadingPlugin)
// Vue.use(infiniteScroll)
// Vue.use(Popup)
// Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
	router,
  render: h => h(App)
}).$mount('#app')

// 路由拦截器
router.beforeEach((to, from, next) => {
	if (to.meta.title) {
		document.title = to.meta.title
	}
	next()
})