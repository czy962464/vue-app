export function getCanvasBase64(img) {
  var image = new Image();
  //至关重要
  image.crossOrigin = '';
  image.src = img;
  //至关重要
  var deferred = $.Deferred();
  if (img) {
    image.onload = function () {
      deferred.resolve(getBase64Image(image));//将base64传给done上传处理
      //document.getElementById("container2").appendChild(image);
    }
    return deferred.promise();//问题要让onload完成后再return sessionStorage['imgTest']
  }
}
function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
  var dataURL = canvas.toDataURL();
  return dataURL;
}

//建立一个可存取到该file的url
export function getObjectURL(file) {
  let url = null;
  if (window.createObjectURL != undefined) {
    url = window.createObjectURL(file);// basic
  } else if (window.URL != undefined) {
    url = window.URL.createObjectURL(file);// mozilla(firefox)
  } else if (window.webkitURL != undefined) {
    url = window.webkitURL.createObjectURL(file);// webkit or chrome
  }
  return url;
}
// 压缩图片
export function compress(img) {
  let canvas = document.createElement("canvas");
  let ctx = canvas.getContext("2d");
  let initSize = img.src.length;
  let width = img.width;
  let height = img.height;
  canvas.width = width;
  canvas.height = height;
  ctx.fillStyle = "#fff";// 铺底色
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(img, 0, 0, width, height);
  let ndata = canvas.toDataURL("image/jpeg", 0.5);//进行最小压缩
  console.log("压缩后的图片大小：" + ndata.length);
  return ndata;
}
// base64转成bolb对象
export function dataURItoBlob(base64Data) {
  var byteString;
  if (base64Data.split(",")[0].indexOf("base64") >= 0)
    byteString = atob(base64Data.split(",")[1]);
  else byteString = unescape(base64Data.split(",")[1]);
  var mimeString = base64Data
    .split(",")[0]
    .split(":")[1]
    .split(";")[0];
  var ia = new Uint8Array(byteString.length);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ia], { type: mimeString });
}