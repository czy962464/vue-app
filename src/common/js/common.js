export function getUrlParamValue(name) {
  if (name == null || name == 'undefined') {return null; }
  var searchStr = decodeURI(window.location.search);
  var infoIndex = searchStr.indexOf(name + "=");
  if (infoIndex == -1) { return null; }
  var searchInfo = searchStr.substring(infoIndex + name.length + 1);
  var tagIndex = searchInfo.indexOf("&");
  if (tagIndex != -1) { searchInfo = searchInfo.substring(0, tagIndex); }
  return searchInfo;
}
export function getUrlAllParam(...args) {
  if (args.length === 0) return undefined
  const url = decodeURIComponent(window.location.href)
  const reg = args.length === 1
   ? new RegExp(`[&?]${args[0]}=([^&%#]+)`)
   : new RegExp(`[&?](?:${args.join('|')})=([^&%#]+)`)
  const matchArray = url.match(reg)
  return matchArray === null ? undefined : matchArray[1]
}